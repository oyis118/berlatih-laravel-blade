<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function store(Request $request){
        //dd($request->all());
        $query = DB::table('pertanyaan')->insert([
            "judul" => $request["title"],
            "isi" => $request["question"],
            "tanggal_dibuat" => now(),
            "tanggal_diperbaharui" => now(),
            "profil_id" => 1,
            "jawaban_tepat_id" => 1
        ]);
        return redirect('/create');
    }
    public function create(){
        return view('pertanyaan.create');
    }
    public function index(){
        $datas =  DB::table('pertanyaan')->get();
        //dd($datas);
        return view('pertanyaan.index', compact('datas'));
    }
    public function show($id){
        $data = DB::table('pertanyaan')->where('id', $id)->first();
        //dd($data);
        return view('pertanyaan.show', compact('data'));
    }
    public function edit($id){
        $data = DB::table('pertanyaan')->where('id', $id)->first();
        //dd($data);
        return view('pertanyaan.edit', compact('data'));
    }
    public function update($id, Request $request){
        $query = DB::table('pertanyaan')->where('id', $id)->update([
            "judul" => $request["title"],
            "isi" => $request["question"]
        ]);
        return redirect('/pertanyaan');
    }
    public function destroy($id){
        $query = DB::table('pertanyaan')->where('id', $id)->delete();
        return redirect('/pertanyaan');
    }
}
